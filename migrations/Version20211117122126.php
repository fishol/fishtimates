<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211117122126 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE estimation (id INT AUTO_INCREMENT NOT NULL, task_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_D05270248DB60186 (task_id), INDEX IDX_D0527024A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning_user (planning_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4545DE073D865311 (planning_id), INDEX IDX_4545DE07A76ED395 (user_id), PRIMARY KEY(planning_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, planning_id INT NOT NULL, INDEX IDX_527EDB253D865311 (planning_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE estimation ADD CONSTRAINT FK_D05270248DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE estimation ADD CONSTRAINT FK_D0527024A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE planning_user ADD CONSTRAINT FK_4545DE073D865311 FOREIGN KEY (planning_id) REFERENCES planning (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE planning_user ADD CONSTRAINT FK_4545DE07A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB253D865311 FOREIGN KEY (planning_id) REFERENCES planning (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE planning_user DROP FOREIGN KEY FK_4545DE073D865311');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB253D865311');
        $this->addSql('ALTER TABLE estimation DROP FOREIGN KEY FK_D05270248DB60186');
        $this->addSql('ALTER TABLE estimation DROP FOREIGN KEY FK_D0527024A76ED395');
        $this->addSql('ALTER TABLE planning_user DROP FOREIGN KEY FK_4545DE07A76ED395');
        $this->addSql('DROP TABLE estimation');
        $this->addSql('DROP TABLE planning');
        $this->addSql('DROP TABLE planning_user');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE user');
    }
}
