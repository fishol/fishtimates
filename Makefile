
project_name = fishtimates

container:
	docker exec -ti $(project_name)_php_1 bash

start:
	docker-compose up -d

stop:
	docker-compose stop

clean:
	docker-compose stop
	docker rm $$(docker ps -aq)
	docker rmi -f $$(docker images -aq)

db:
	docker exec -ti $(project_name)_mysql_1 mysql -uroot -proot app

migrate:
	docker exec -ti $(project_name)_php_1 php bin/console doctrine:migrations:migrate

new-migration:
	docker exec -ti $(project_name)_php_1 php bin/console doctrine:migrations:diff

fixtures:
	docker exec -ti $(project_name)_php_1 php bin/console doctrine:fixtures:load --no-interaction

docs:
	docker run --rm --name=symfony_docs -p 8080:80 17710472946/symfony-docs:v1

