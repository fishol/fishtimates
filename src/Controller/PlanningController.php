<?php

namespace App\Controller;

use App\Entity\Planning;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;


/**
 * @Route("/planning")
 */
class PlanningController extends AbstractController
{

    /**
     * @Route("/", name="planning_list")
     */
    public function index(Security $security): Response
    {
        $repo = $this->getDoctrine()->getRepository(Planning::class);

        $plannings =
            $this->isGranted('ROLE_ADMIN') ? $repo->findAll() :
                $this->isGranted('ROLE_MANAGER') ? $repo->findAll() :
                    $this->isGranted('ROLE_USER') ?? $repo->findAll();

        return $this->render('planning/index.html.twig', [
            'plannings' => $plannings
        ]);
    }

    /**
     * @Route("/show/{id}", name="planning_show")
     */
    public function show(Planning $planning): Response
    {
        return $this->render('planning/show.html.twig', [
            'test' => 'dupa sraka',
        ]);
    }

    /**
     * @Route("/create", name="planning_create")
     */
    public function create(): Response
    {
        $planning = new Planning();
        $om = $this->getDoctrine()->getManager();
        $om->persist($planning);
        $om->flush();

        return $this->redirectToRoute('planning_show', ['id' => $planning->getId()]);
    }

    /**
     * @Route("/close", name="planning_close")
     */
    public function close()
    {

    }

    /**
     * @Route("/add_member", name="planning_add_member")
     */
    public function addMember()
    {

    }

    /**
     * @Route("/remove_member", name="planning_remove_member")
     */
    public function removeMember()
    {

    }
}
