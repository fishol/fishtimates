<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\NewUserType;
use App\Form\UserPasswordType;
use App\Form\UserType;
use App\Service\UserFormService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_list")
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/create", name="user_create")
     * @IsGranted("ROLE_MANAGER")
     */
    public function create(Request $request, UserFormService $formService): Response
    {
        $user = new User();
        $form = $this->createForm(NewUserType::class, $user);

        $form->handleRequest($request);

        $isCreated = $formService->create($form);

        if ($isCreated) {
            $this->addFlash('success', 'User ' . $user->getUserIdentifier() . ' created');

            return $this->redirectToRoute('user_list');
        }

        return $this->renderForm('user/create.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="user_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(User $user): Response
    {
        if ($user->getId() === $this->getUser()->getId()) {
            $this->addFlash('warning', 'You shouldn\'t try to delete yourself...');

            return $this->redirectToRoute('user_list');
        }

        $om = $this->getDoctrine()->getManager();
        $om->remove($user);
        $om->flush();

        $this->addFlash('success', 'User ' . $user->getUserIdentifier() . ' is deleted');

        return $this->redirectToRoute('user_list');
    }

    /**
     * @Route("/edit/{id}", name="user_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'User ' . $user->getUserIdentifier() . ' is updated');

            return $this->redirectToRoute('user_list');
        }

        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form
        ]);
    }

    /**
     * @Route("/change_password/{id}", name="user_change_password")
     * @IsGranted("ROLE_ADMIN")
     */
    public function changePassword(
        Request $request,
        User $user,
        UserFormService $formService
    ): Response
    {
        $form = $this->createForm(UserPasswordType::class);
        $form->handleRequest($request);

        $isUpdated = $formService->updatePassword($user, $form);

        if ($isUpdated) {
            $this->addFlash('success', 'Password for ' . $user->getUserIdentifier() . ' user is updated');

            return $this->redirectToRoute('user_list');
        }

        return $this->renderForm('user/change_password.html.twig', [
            'form' => $form
        ]);
    }
}
