<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewUserType extends UserType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('password', UserPasswordType::class, [
                'mapped' => false,
            ])
//            ->add('password', PasswordType::class, [
//                'always_empty' => true,
//                'mapped' => false,
//                'attr' => [
//                    'autocomplete' => 'new-password',
//                ]
//            ])
//            ->add('passwordConfirm', PasswordType::class, [
//                'always_empty' => true,
//                'mapped' => false,
//            ])
        ;
    }

}
