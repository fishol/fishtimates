<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class)
            ->add('roles', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'Manager' => 'ROLE_MANAGER',
                    'User' => 'ROLE_USER'
                ],
            ])
            ->add('submit', SubmitType::class)
        ;

        $builder->get('roles')->addModelTransformer(new CallbackTransformer(
            function ($array) {
                if (in_array('ROLE_ADMIN', $array)) return 'ROLE_ADMIN';
                if (in_array('ROLE_MANAGER', $array)) return 'ROLE_MANAGER';
                return 'ROLE_USER';
            },
            function ($string) {
                return [$string];
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
