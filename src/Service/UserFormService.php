<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\AppException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFormService
{

    /** @var UserPasswordHasherInterface $hasher */
    private $hasher;

    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * @param UserPasswordHasherInterface $passwordHasher
     * @param EntityManagerInterface $em
     */
    public function __construct(UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $em)
    {
        $this->hasher = $passwordHasher;
        $this->em = $em;
    }

    /**
     * @param User $user
     * @param FormInterface $form
     * @return bool
     */
    public function updatePassword(User $user, FormInterface $form): bool
    {
        if (!$form->isSubmitted() || !$form->isValid()) {
            return false;
        }

        $password = $form->get('password')->getData();
        $passwordConfirmation = $form->get('passwordConfirm')->getData();

        try {
            $this->setPasswordOnUser($user, $password, $passwordConfirmation);
        } catch (AppException $exception) {
            $form->get('password')->addError(new FormError($exception->getMessage()));

            return false;
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param FormInterface $form
     * @return bool
     */
    public function create(FormInterface $form): bool
    {
        if (!$form->isSubmitted() || !$form->isValid()) {
            return false;
        }

        /** @var User $user */
        $user = $form->getData();
        $password = $form->get('password')->get('password')->getData();
        $passwordConfirmation = $form->get('password')->get('passwordConfirm')->getData();

        try {
            $this->setPasswordOnUser($user, $password, $passwordConfirmation);
        } catch (AppException $exception) {
            $form->get('password')->get('password')->addError(new FormError($exception->getMessage()));

            return false;
        }

        $this->em->persist($user);
        $this->em->flush();

        return true;

    }


    /**
     * @param User $user
     * @param string $password
     * @param string $confirmation
     * @throws AppException
     */
    private function setPasswordOnUser(User $user, string $password, string $confirmation)
    {
        if ($password !== $confirmation) {
            throw new AppException('Passwords doesn\'t match');
        }

        $passwordEncoded = $this->hasher->hashPassword($user, $password);
        $user->setPassword($passwordEncoded);
    }

}